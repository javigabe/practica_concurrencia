// Grupo: Javier Galarza Becker (z170071)

import es.upm.babel.cclib.Monitor;


public class EnclavamientoMonitor implements Enclavamiento {

  Monitor mutex = new Monitor();

  // condiciones de bloqueo
  private Monitor.Cond condCambioBarreraA = mutex.newCond();
  private Monitor.Cond condCambioBarreraC = mutex.newCond();
  private Monitor.Cond condCambioFrenoA = mutex.newCond();
  private Monitor.Cond condCambioFrenoD = mutex.newCond();
  private Monitor.Cond condCambioSemaforo1 = mutex.newCond();
  private Monitor.Cond condCambioSemaforo2 = mutex.newCond();
  private Monitor.Cond condCambioSemaforo3 = mutex.newCond();

  // variables para el control del desbloqueo de los semaforos
  private Control.Color color1;
  private Control.Color color2;
  private Control.Color color3;

  // variable para el control del desbloqueo del freno
  private boolean activo;

  // variables del recurso
  boolean presencia;
  private Integer[] tren = new Integer[4];
  private Control.Color[] color = new Control.Color[4];


  public EnclavamientoMonitor() {
    // iniciliazacion del recurso
    presencia = false;

    for (int i = 0; i < 4; i++) {
        // tren[0] y color[0] son valores espureos
        tren[i] = 0;
        color[i] = Control.Color.VERDE;
      }
  }
  
  @Override
  public void avisarPresencia(boolean presencia) {
    mutex.enter();

    // Implementacion de la POST
    this.presencia = presencia;
    coloresCorrectos();

    // codigo de desbloqueo
    desbloquear();

    mutex.leave();
  }

  @Override
  public boolean leerCambioBarrera(boolean actual) {
    mutex.enter();
    boolean esperado;

    // chequeo de la CPRE y posible bloqueo

    if (actual == (tren[1] + tren[2] == 0)) {
      if (!actual) {
        condCambioBarreraA.await();
      }
      else {
        condCambioBarreraC.await();
      }
    }

    // implementacion de la POST
    esperado = tren[1] + tren[2] == 0;

    // codigo de desbloqueo
    desbloquear();

    mutex.leave();
    return esperado;
  }

  @Override
  public boolean leerCambioFreno(boolean actual) {
    boolean esperado;
    mutex.enter();

    // chequeo de la CPRE y posible bloqueo
    activo = actual;

    if (actual == (tren[1] > 1 || tren[2] > 1 || (tren[2] == 1 && presencia))) {
      if (!actual) {
        condCambioFrenoA.await();
      }
      else {
        condCambioFrenoD.await();
      }
    }

    // implementacion de la POST
    esperado = tren[1] > 1 || tren[2] > 1 || (tren[2] == 1 && presencia);

    // codigo de desbloqueo
    desbloquear();

    mutex.leave();
    return esperado;
  }

  @Override
  public Control.Color leerCambioSemaforo(int i, Control.Color actual) {
    // chequeo de la PRE
    if (i == 0 || i > 3) {
      throw new PreconditionFailedException("Semaforo 0 no existe");
    }

    mutex.enter();
    Control.Color esperado;

    // chequeo de la CPRE y posible bloqueo
    if (actual == color[i]) {
      if (i == 1) {
        color1 = actual;
        condCambioSemaforo1.await();
      }
      else if (i == 2) {
        color2 = actual;
        condCambioSemaforo2.await();
      }
      else {
        color3 = actual;
        condCambioSemaforo3.await();
      }
    }
    // implementacion de la POST
    esperado = color[i];

    // codigo de desbloqueo
    desbloquear();

    mutex.leave();
    return esperado;
  }

  @Override
  public void avisarPasoPorBaliza(int i) {
    // chequeo de la PRE
    if (i == 0) {
      throw new PreconditionFailedException("Baliza 0 no existe");
    }

    mutex.enter();

    // implementacion de la POST
    tren[i - 1] = tren[i - 1] - 1;
    tren[i] = tren[i] + 1;

    coloresCorrectos();

    // codigo de desbloqueo
    desbloquear();
    mutex.leave();
  }


  private void coloresCorrectos() {
    // METODO AUXILIAR PARA EL AJUSTE DE LOS COLORES DE LOS SEMAFOROS

    if (tren[1] > 0) {
      color[1] = Control.Color.ROJO;
    }
    else if (tren[1] == 0 && (tren[2] > 0 || presencia)) {
      color[1] = Control.Color.AMARILLO;
    }
    else if (tren[1] == 0 && tren[2] == 0 && !presencia) {
      color[1] = Control.Color.VERDE;
    }
    if (tren[2] > 0 || presencia) {
      color[2] = Control.Color.ROJO;
    }
    else if (tren[2] == 0 && !presencia) {
      color[2] = Control.Color.VERDE;
    }
    color[3] = Control.Color.VERDE;
  }


  private void desbloquear() {
    // METODO AUXILIAR DE DESBLOQUEO

    // variable boolean para evitar hacer varios signals juntos
    boolean desbloqueado = false;

    // variable boolean para comprobar cada cpre
    boolean cpre = color1 != color[1];


    // codigo de desbloqueo del semaforo 1
    if (cpre && condCambioSemaforo1.waiting() > 0) {
      condCambioSemaforo1.signal();
      desbloqueado = true;
    }

    // codigo de desbloqueo del semaforo 2
    cpre = color2 != color[2];
    if (cpre && condCambioSemaforo2.waiting() > 0 && !desbloqueado) {
      condCambioSemaforo2.signal();
      desbloqueado = true;
    }

    // codigo de desbloqueo del semaforo 3
    cpre = color3 != color[3];
    if (cpre && condCambioSemaforo3.waiting() > 0 && !desbloqueado) {
      condCambioSemaforo3.signal();
      desbloqueado = true;
    }

    // codigo de desbloqueo para abrir la barrera
    cpre = tren[1] + tren[2] == 0;
    if (cpre && condCambioBarreraA.waiting() > 0 && !desbloqueado) {
      condCambioBarreraA.signal();
      desbloqueado = true;
    }

    // codigo de desbloqueo para cerrar la barrera
    else if (!cpre){
      if (condCambioBarreraC.waiting() > 0 && !desbloqueado) {
        condCambioBarreraC.signal();
        desbloqueado = true;
      }
    }

    // codigo de desbloqueo para activar el freno
    cpre = tren[1] > 1 || tren[2] > 1 || (tren[2] == 1 && presencia);
    if (cpre && condCambioFrenoA.waiting() > 0 && !desbloqueado) {
      if (!activo) {
        condCambioFrenoA.signal();
        desbloqueado = true;
      }
    }

    // codigo de desbloqueo para desactivar el freno
    else if (!cpre && condCambioFrenoD.waiting() > 0 && !desbloqueado){
      if (activo) {
        condCambioFrenoD.signal();
      }
    }
  }


 
}
