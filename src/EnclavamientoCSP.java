// Grupo: Javier Galarza Becker (z170071)

import es.upm.aedlib.fifo.FIFO;
import es.upm.aedlib.fifo.FIFOList;
import org.jcsp.lang.Alternative;
import org.jcsp.lang.Any2OneChannel;
import org.jcsp.lang.CSProcess;
import org.jcsp.lang.ProcessManager;
import org.jcsp.lang.Channel;
import org.jcsp.lang.Guard;
import org.jcsp.lang.One2OneChannel;

/**
 * Implementation using CSP (Mixed).
 * TÃ©cnica de peticiones aplazadas
 * excepto en las ops. de aviso (no bloqueantes)
 *
 * @author rul0
 */
public class EnclavamientoCSP implements CSProcess, Enclavamiento {

    /** WRAPPER IMPLEMENTATION */
    //** Channels for receiving external requests
    // Un canal por op. del recurso
    private final Any2OneChannel chAvisarPresencia     = Channel.any2one();
    private final Any2OneChannel chLeerCambioBarrera   = Channel.any2one();
    private final Any2OneChannel chLeerCambioFreno     = Channel.any2one();
    private final Any2OneChannel chLeerCambioSemaforo  = Channel.any2one();
    private final Any2OneChannel chAvisarPasoPorBaliza = Channel.any2one();

    public EnclavamientoCSP() {
        new ProcessManager(this).start();
    }

    // Clases auxiliares para las peticiones que se envÃ­an al servidor
    public static class PeticionLeerCambioBarrera{
        protected One2OneChannel channel;
        protected boolean value;

        public PeticionLeerCambioBarrera(One2OneChannel channel, boolean value) {
            this.channel = channel;
            this.value = value;
        }
    }

    public static class PeticionLeerCambioFreno{
        protected One2OneChannel channel;
        protected boolean value;

        public PeticionLeerCambioFreno(One2OneChannel channel, boolean value) {
            this.channel = channel;
            this.value = value;
        }
    }

    public static class PeticionLeerCambioSemaforo{
        protected One2OneChannel channel;
        protected Control.Color color;
        protected int index;

        public PeticionLeerCambioSemaforo(One2OneChannel channel,
                                          Control.Color color,
                                          int index) {
            this.channel = channel;
            this.color = color;
            this.index = index;
        }
    }

    // ImplementaciÃ³n de la interfaz Enclavamiento
    @Override
    public void avisarPresencia(boolean presencia) {
        chAvisarPresencia.out().write(presencia);
    }

    @Override
    public boolean leerCambioBarrera(boolean abierta) {
        One2OneChannel ch = Channel.one2one();
        chLeerCambioBarrera.out().write(new PeticionLeerCambioBarrera(ch, abierta));

        return (Boolean) ch.in().read();
    }

    @Override
    public boolean leerCambioFreno(boolean accionado) {
        One2OneChannel ch = Channel.one2one();
        chLeerCambioFreno.out().write(new PeticionLeerCambioFreno(ch, accionado));

        return (Boolean) ch.in().read();
    }

    /** notice that exceptions can be thrown outside the server */
    @Override
    public Control.Color leerCambioSemaforo(int i, Control.Color color) {
        if (i == 0 )
            throw new PreconditionFailedException("Semaforo 0 no existe");

        One2OneChannel ch = Channel.one2one();
        chLeerCambioSemaforo.out().write(new PeticionLeerCambioSemaforo(ch, color, i));

        return (Control.Color) ch.in().read();
    }

    @Override
    public void avisarPasoPorBaliza(int i) {
        if (i == 0 )
            throw new PreconditionFailedException("Baliza 0 no existe");

        chAvisarPasoPorBaliza.out().write(i);
    }


    /** SERVER IMPLEMENTATION */
    static final int AVISAR_PRESENCIA = 0;
    static final int LEER_CAMBIO_BARRERA = 1;
    static final int LEER_CAMBIO_FRENO  = 2;
    static final int LEER_CAMBIO_SEMAFORO  = 3;
    static final int AVISAR_PASO_POR_BALIZA = 4;

    @Override
        public void run() {
            // variables del recurso
            Boolean presencia;
            Integer[] tren = new Integer[4];
            Control.Color[] color = new Control.Color[4];


            // Inicializacion del recurso
            // tren[0] y color[0] son valores espureos
            for (int i = 0; i < tren.length; i++) {
                tren[i] = 0;
                color[i] = Control.Color.VERDE;
            }
            presencia = false;



            //  Estructuras auxiliares para guardar las peticiones aplazadas
            PeticionLeerCambioBarrera esperaBarrera = null;
            FIFO<Object> esperanSemaforo = new FIFOList<Object>();
            PeticionLeerCambioFreno esperaFreno = null;

            // canal de respuesta a las peticiones aplazadas
            One2OneChannel chResp;

            // Construccion de la recepcion alternativa
            Guard[] inputs = {
                    chAvisarPresencia.in(),
                    chLeerCambioBarrera.in(),
                    chLeerCambioFreno.in(),
                    chLeerCambioSemaforo.in(),
                    chAvisarPasoPorBaliza.in()
            };

            Alternative services = new Alternative(inputs);
            int chosenService = 0;

            // Bucle de servicio
            while (true){
                chosenService = services.fairSelect();

                switch (chosenService) {
                    case AVISAR_PRESENCIA:
                        // lectura de la peticion e implementacion de la post
                        presencia = (Boolean) chAvisarPresencia.in().read();
                        color = coloresCorrectos(tren, color, presencia);
                        break;

                    case LEER_CAMBIO_BARRERA:
                        // lectura de la peticion y guardada en la estructura auxiliar
                        esperaBarrera = (PeticionLeerCambioBarrera) chLeerCambioBarrera.in().read();
                        break;

                    case LEER_CAMBIO_FRENO:
                        // lectura de la peticion y guardada en la estructura auxiliar
                        esperaFreno =  (PeticionLeerCambioFreno) chLeerCambioFreno.in().read();
                        break;

                    case LEER_CAMBIO_SEMAFORO:
                        // lectura de la peticion y guardada en la estructura auxiliar
                        Object pet_semaforo = chLeerCambioSemaforo.in().read();
                        esperanSemaforo.enqueue(pet_semaforo);
                        break;

                    case AVISAR_PASO_POR_BALIZA:
                        // lectura de la peticion e implementacion de la post
                        Integer i = (Integer) chAvisarPasoPorBaliza.in().read();
                        tren[i - 1] = tren[i - 1] - 1;
                        tren[i] = tren[i] + 1;

                        color = coloresCorrectos(tren, color, presencia);
                        break;
                } // switch


                // codigo de desbloqueo

                // bucle sobre las peticiones de desbloqueo de semaforos aplazadas
                for (int i = 0; i < esperanSemaforo.size(); i++) {
                    PeticionLeerCambioSemaforo pet_semaforo = (PeticionLeerCambioSemaforo) esperanSemaforo.first();

                    chResp = pet_semaforo.channel;
                    Integer index = pet_semaforo.index;

                    if (condicionDesbloqueo(pet_semaforo, tren, color, presencia)) {
                        chResp.out().write(color[index]);
                        esperanSemaforo.dequeue();
                        i--;
                    }

                    else {
                        esperanSemaforo.dequeue();
                        esperanSemaforo.enqueue(pet_semaforo);
                    }
                }

                // comprobacion sobre la variable esperaBarrera, que guarda la peticion aplazada sobre la barrera
                if (esperaBarrera != null) {
                    if (condicionDesbloqueo(esperaBarrera, tren, color, presencia)) {
                        chResp = esperaBarrera.channel;
                        chResp.out().write(tren[1] + tren[2] == 0);
                        esperaBarrera = null;
                    }
                }

                // comprobacion sobre la variable esperaFreno, que guarda la peticion aplazada sobre el freno
                if (esperaFreno != null) {
                    if (condicionDesbloqueo(esperaFreno, tren, color, presencia)) {
                        chResp = esperaFreno.channel;
                        chResp.out().write(tren[1] > 1 || tren[2] > 1 || (tren[2] == 1 && presencia));
                        esperaFreno = null;
                    }
                }


            } // end bucle servicio
        } // end run method

    private Control.Color[] coloresCorrectos(Integer[] tren, Control.Color[] color, boolean presencia) {
        // METODO AUXILIAR PARA EL AJUSTE DE COLORES
        if (tren[1] > 0) {
            color[1] = Control.Color.ROJO;
        }
        else if (tren[1] == 0 && (tren[2] > 0 || presencia)) {
            color[1] = Control.Color.AMARILLO;
        }
        else if (tren[1] == 0 && tren[2] == 0 && !presencia) {
            color[1] = Control.Color.VERDE;
        }
        if (tren[2] > 0 || presencia) {
            color[2] = Control.Color.ROJO;
        }
        else if (tren[2] == 0 && !presencia) {
            color[2] = Control.Color.VERDE;
        }
        color[3] = Control.Color.VERDE;
        return color;
    }

    private boolean condicionDesbloqueo (Object peticion, Integer[] tren, Control.Color[] color, boolean presencia) {
        // METODO AUXILIAR PARA LA LECTURA DE LAS PETICIONES APLAZADAS

        // codigo para la comprobacion de la CPRE de las peticiones de semaforo aplazadas

        if (peticion instanceof PeticionLeerCambioSemaforo) {
            PeticionLeerCambioSemaforo pet = (PeticionLeerCambioSemaforo) peticion;
            Control.Color col = pet.color;
            int index = pet.index;

            // chequeo de la CPRE
            if (color[index] != col) {
                return true;
            }
            return false;
        }

        // codigo para la comprobacion de la CPRE de las peticiones de barrera aplazadas

        else if (peticion instanceof PeticionLeerCambioBarrera) {
            PeticionLeerCambioBarrera pet = (PeticionLeerCambioBarrera) peticion;
            Boolean abierta = pet.value;

            // chequeo de la CPRE
            if (abierta != (tren[1] + tren[2] == 0)) {
                return true;
            }
            return false;
        }

        // codigo para la comprobacion de la CPRE de las peticiones de freno aplazadas

        else {
            PeticionLeerCambioFreno pet = (PeticionLeerCambioFreno) peticion;

            Boolean activo = pet.value;

            // chequeo de la CPRE
            if (activo != (tren[1] > 1 || tren [2] > 1 || (tren[2] == 1 && presencia))) {
                return true;
            }
            return false;
        }
    }

} // end CLASS